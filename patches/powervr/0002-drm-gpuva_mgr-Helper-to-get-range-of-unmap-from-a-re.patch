From 3af04293b67aee64bf2ef960024820100904d45f Mon Sep 17 00:00:00 2001
From: Donald Robson <donald.robson@imgtec.com>
Date: Wed, 6 Sep 2023 10:55:24 +0100
Subject: [PATCH 02/20] drm/gpuva_mgr: Helper to get range of unmap from a
 remap op.

Signed-off-by: Donald Robson <donald.robson@imgtec.com>
---
 include/drm/drm_gpuva_mgr.h | 27 +++++++++++++++++++++++++++
 1 file changed, 27 insertions(+)

diff --git a/include/drm/drm_gpuva_mgr.h b/include/drm/drm_gpuva_mgr.h
index ed8d50200cc3..be7b3a6d7e67 100644
--- a/include/drm/drm_gpuva_mgr.h
+++ b/include/drm/drm_gpuva_mgr.h
@@ -703,4 +703,31 @@ void drm_gpuva_remap(struct drm_gpuva *prev,
 
 void drm_gpuva_unmap(struct drm_gpuva_op_unmap *op);
 
+/**
+ * drm_gpuva_op_remap_get_unmap_range() - Helper to get the start and range of
+ * the unmap stage of a remap op.
+ * @op: Remap op.
+ * @start_addr: Output pointer for the start of the required unmap.
+ * @range: Output pointer for the length of the required unmap.
+ *
+ * These parameters can then be used by the caller to unmap memory pages that
+ * are no longer required.
+ */
+static __always_inline void
+drm_gpuva_op_remap_get_unmap_range(const struct drm_gpuva_op_remap *op,
+				   u64 *start_addr, u64 *range)
+{
+	const u64 va_start = op->prev ?
+			     op->prev->va.addr + op->prev->va.range :
+			     op->unmap->va->va.addr;
+	const u64 va_end = op->next ?
+			   op->next->va.addr :
+			   op->unmap->va->va.addr + op->unmap->va->va.range;
+
+	if (start_addr)
+		*start_addr = va_start;
+	if (range)
+		*range = va_end - va_start;
+}
+
 #endif /* __DRM_GPUVA_MGR_H__ */
-- 
2.39.2

